from django import forms


class QuizForm(forms.Form):

    def __init__(self, questions, quiz_pk, *args, **kwargs):
        super(QuizForm, self).__init__(*args, **kwargs)
        for index, question in enumerate(questions):
            self.fields['question_' + str(index)] = forms.ModelChoiceField(
                label=question['question'],
                queryset=question['question'].choice_set.all().order_by('number_in_question'),
                required=True, initial=1,
                widget=forms.RadioSelect()
            )
        self.fields['quiz_pk'] = forms.IntegerField(initial=quiz_pk, widget=forms.HiddenInput)
