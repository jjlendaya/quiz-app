from django.contrib import admin

from .models import Quiz, QuizSession, QuizSessionAnswers, Question, Choice

# Register your models here.
admin.site.register(Quiz)


@admin.register(Choice)
class ChoiceAdmin(admin.ModelAdmin):
    list_display = ['get_quiz', 'question', 'label', 'correct']
    fields = ['label', 'correct', 'question']


class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 0
    exclude = ['number_in_question']


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ['get_quiz', 'question']
    inlines = [ChoiceInline]
