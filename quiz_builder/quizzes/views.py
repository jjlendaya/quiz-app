from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views import generic
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from datetime import datetime as dt
from django.urls import reverse
from django.http import HttpResponse

from .models import Quiz, QuizSession, QuizSessionAnswers
from .forms import QuizForm


@login_required
def quiz_results(request, quiz_pk):
    quiz = get_object_or_404(Quiz, pk=quiz_pk)
    session = QuizSession.objects.filter(user=request.user, quiz=quiz)
    if session.count() < 1:
        return redirect(reverse('quizzes:quiz-detail', args=[str(quiz.id)]), {
            'error_message': 'You have not taken this quiz yet. Want to try it out?'
        })
    else:
        user_results = []
        all_questions = session[0].quiz.question_set.all()
        for index, question in enumerate(all_questions):
            correct_answer = question.choice_set.filter(correct=True)[0]
            user_answer = session[0].quizsessionanswers_set.all()[index].answer
            user_results.append({
                'question': question,
                'correct_answer': correct_answer,
                'user_answer': user_answer,
                'correct': correct_answer == user_answer,
                'choices': question.choice_set.all()
            })
        total_questions = all_questions.count()
        return render(
            request,
            'quizzes/quiz_result.html',
            context={
                'results': user_results,
                'quiz': quiz,
                'score': session[0].result,
                'total_questions': total_questions
            }
        )


# Create your views here.
@login_required
def take_quiz(request, pk):
    quiz = get_object_or_404(Quiz, pk=pk)
    session = QuizSession.objects.filter(user=request.user, quiz=quiz)
    print(session)
    if session.count() < 1:
        questions = []
        for question in quiz.question_set.all():
            choices = question.choice_set.all()
            answers = choices.filter(correct=True)
            questions.append({
                'question': question,
                'choices': choices,
                'answers': answers
            })

        if quiz.expiration_date is not None and dt.now().date() > quiz.expiration_date:
            return redirect(reverse('quizzes:index'),
                            {'error_message': 'You cannot take this quiz because it has expired.'})

        if request.method == 'POST':
            score = 0
            answers = []
            form = QuizForm(questions, quiz.id, data=request.POST)
            if form.is_valid():
                for index, question in enumerate(questions):
                    correct_choice = question['question'].choice_set.filter(correct=True)[0]
                    user_choice = form.cleaned_data['question_' + str(index)]
                    if user_choice.pk == correct_choice.pk:
                        score += 1
                    answers.append({
                        'question': question['question'],
                        'answer': user_choice
                    })
                new_quiz_session = QuizSession(
                    user=request.user,
                    quiz_date_taken=dt.now().date,
                    result=score,
                    quiz=quiz
                )
                new_quiz_session.save()
                for answer in answers:
                    new_quiz_session_answer = QuizSessionAnswers(
                        quiz_session=new_quiz_session,
                        question=answer['question'],
                        answer=answer['answer']
                    )
                    new_quiz_session_answer.save()
                return redirect(reverse('quizzes:quiz-results', args=[str(quiz.id)]))

        form = QuizForm(questions, quiz.id)
        return render(request, 'quizzes/quiz_take.html', context={
            'quiz': quiz,
            'form': form
        })
    else:
        return redirect(reverse('quizzes:quiz-results', args=[str(quiz.id)]))


class QuizListView(generic.ListView):
    model = Quiz
    paginate_by = 20
    ordering = ['date_created']

    def get_context_data(self, **kwargs):
        context = super(QuizListView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            sessions = QuizSession.objects.filter(user=self.request.user).order_by('-quiz_date_taken')
            quiz_list = []
            for session in sessions:
                quiz_list.append({
                    'quiz': session.quiz,
                    'url': session.quiz.get_absolute_url()
                })
            print(quiz_list[:10])
            context['user_quiz_list'] = quiz_list[:10]
        return context


class QuizDetailView(generic.DetailView):
    model = Quiz

    def get_context_data(self, **kwargs):
        context = super(QuizDetailView, self).get_context_data(**kwargs)
        context['number_of_questions'] = self.object.question_set.count()
        return context
