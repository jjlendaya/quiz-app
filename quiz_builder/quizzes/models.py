from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse


# Create your models here.
class Quiz(models.Model):
    title = models.CharField(max_length=200)
    date_created = models.DateField(auto_now_add=True)
    expiration_date = models.DateField(null=True, blank=True)

    class Meta:
        verbose_name = 'Quiz'
        verbose_name_plural = 'Quizzes'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('quizzes:quiz-detail', args=[str(self.id)])


class Question(models.Model):
    question = models.CharField(max_length=200)
    quiz = models.ForeignKey('Quiz', on_delete=models.CASCADE)

    def __str__(self):
        return self.question

    def get_quiz(self):
        return self.quiz
    get_quiz.short_description = 'Quiz'


class Choice(models.Model):
    label = models.CharField(max_length=150)
    correct = models.BooleanField(default=False)
    number_in_question = models.IntegerField(default=1)
    question = models.ForeignKey('Question', on_delete=models.CASCADE)

    def __str__(self):
        return self.label

    def get_quiz(self):
        return self.question.quiz
    get_quiz.short_description = 'Quiz'

    def save(self, *args, **kwargs):
        if self._state.adding:
            last_number_in_question = Choice.objects.filter(question=self.question).aggregate(largest=models.Max('number_in_question'))['largest']

            if last_number_in_question is not None:
                self.number_in_question = last_number_in_question + 1

        super(Choice, self).save(*args, **kwargs)


class QuizSession(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    quiz = models.ForeignKey('Quiz', on_delete=models.CASCADE)
    quiz_date_taken = models.DateField(auto_now_add=True)
    result = models.IntegerField(default=0)

    def __str__(self):
        return str(self.quiz) + ' - ' + str(self.user)


class QuizSessionAnswers(models.Model):
    quiz_session = models.ForeignKey('QuizSession', on_delete=models.CASCADE)
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    answer = models.ForeignKey('Choice', on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return str(self.question) + ' - ' + str(self.answer)
