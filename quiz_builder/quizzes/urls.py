from django.conf.urls import url

from . import views

app_name = 'quizzes'
urlpatterns = [
    url(r'^$', views.QuizListView.as_view(), name='index'),
    url(r'^(?P<pk>\d+)$', views.QuizDetailView.as_view(), name='quiz-detail'),
    url(r'^(?P<pk>\d+)/take/', views.take_quiz, name='take-quiz'),
    url(r'^(?P<quiz_pk>\d+)/results/', views.quiz_results, name='quiz-results'),
]
